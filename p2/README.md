# LIS4369 Extensible Enterprise Solutions

## Alec Elsbernd

### Project 2 Requirements:

*Four Parts:*

1. Requirements:
	1. Use assignment 5 screenshots and references above for the following requirements:
	2. Backward-engineer the lis4369_p2_requirements.txt file
	3. Be sure to include at least two plots in your README.md file.
2. Be sure to test your program using RStudio
3. Questions
4. Bitbucket repo links:
	1. this assignment 

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots of output from code below.

#### Assignment Screenshots:

*Screenshot of lis4369_p2.R application:*

![lis4369_p2.R](img/p2.1.PNG)Â ![lis4369_p2.R](img/p2.2.PNG)![lis4369_p2.R](img/p2.3.PNG)Â ![lis4369_p2.R](img/p2.4.PNG)
![lis4369_p2.R](img/p2.5.PNG)Â ![lis4369_p2.R](img/p2.6.PNG)![lis4369_p2.R](img/p2.7.PNG)Â


*Screenshot of plots after running application:*

![Plots after running App](img/plot_disp_and_mpg_1.png) ![Plots after running App](img/plot_disp_and_mpg_2.png)


#### Links:

*a) Assignment:*
[P2 Link](https://bitbucket.org/ame18s/lis4369/src/master/p2/)