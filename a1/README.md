# LIS4369 Advanced Database Analysis

## Alec Elsbernd


### Assignment #1 Requirements:
- Install Python
- Install R
- Install R Studio
- Install Visual Studio Code
- Create a1_tip_calculator application
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Butbucket tutorial
- Provide git command descriptions

> #### Git commands w/short descriptions:

1. git init - creates a new repo or initialize an empty repo
2. git status -  shows the state of the working directory and the staging area
3. git add - adds all modified and new files in the current directory and prepares them to be pulled
4. git commit - records the changes you made to the local repo
5. git push - updates the remote repo with local changes
6. git pull - changes your local repo to that made on the remote repo
7. git branch - list branches that are connected

#### Assignment Screenshots:

*Screenshot code and output:*

![assignment 1 code and output](img/lis4369_a1_pic1.PNG)
*Screenshot of a1 code and output*:

![assignment 1 isntall proof](img/a1_python.PNG)

*Screenshot of Python Install proof*

![assignment 1 isntall proof](img/a1_rstudio.PNG)

*Screenshot of Rstudio Install proof*

