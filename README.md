# LIS4369 Extensible Interprise Solutions

## Alec Elsbernd

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	
   	* Install Python
   	* Install R
   	* Install R Studio
   	* Install Visual Studio Code
   	* Create a1_tip_calculator application
   	* Provide screenshots of installations
   	* Create Bitbucket repo
   	* Complete Butbucket tutorial
   	* Provide git command descriptions
	
2. [A2 README.md](a2/README.md "My A2 README.md file")
	
   	* Must use float data type for user input.
   	* Overtime rate: 1.5 times hourly rate (hours over 40).
   	* Holiday rate: 2.0 times hourly rate (all holiday hours).
   	* Must format currency with dollar sign, and round to two decimal places.
   	* Create at least three functions that are called by the program
	  	 * main(): calls at least two other functions.
	  	 * get_requirements(): displays the program requirements.
	  	 * calculate_payroll(): calculates an individual one*week paycheck.)
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	
  	* Calculate home interior paint cost (w/o primer).
   	* Must use float data type.
  	* Must use SQFT_PER_GALLON constant (350).
  	* Must use iteration structure (aka loop).
   	* Format, right*align numbers, and round to two decimal places.
  	* Create at least five functions that are called by the program:
	   	* main(): calls two other functions: get_requirments() and estimate_painting_cost().
	   	* get_requirments(): displays the program requirments.
	   	* estimate_painting_cost(): calculates interior home paintingm and calls print functions.
	   	* print_painting_estimate(): displays painting costs.
	   	* print_painting_percentage(): displays painting costs persentages.
	
4. [P1 README.md](p1/README.md "My P1 README.md file")

	* Run demo.py.
 	* If erros, more than likely missing installations.
 	* Test Python Package Installer: pip freeze
 	* Reserach how to do the following intsallations:
 		* pandas (only if missing) 
 		* pandas-datareader (only if missing)
 		* Matplotlib (only if missing)
 	* Create at least three functions that are called by the program:
 		* main(): calls at least two other fuctions 
 		* get_requirments(): displays the program requirements.
 		* data_analysis_1(): displaysthe following data.

	
5. [A4 README.md](a4/README.md "My A4 README.md file")

	* Run demo.py.
	* If errors, more than likely missing installations.\n"
	* Test Python Package Installer: pip freeze.\n"
	* Research how to install any missing packages:\n"
	* Create at least three functions that are called by the program:\n"
		* main(): calls atleast two other functions.\n"
		* get_requirements(): displays the program requirements.\n"
		* data_analysis_2(): displays results as per demo.py.\n"
	* Display graph as per instructions w/in demo.py.")

6. [A5 README.md](https://bitbucket.org/ame18s/lis4369/src/master/a5/README.md)

	* Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
	* Code and run lis4369_a5.R
	* Be sure to include at least two plots in your README.md file.
	* Be sure to test your program using RStudio
	
7. [P2 README.md](p2/README.md "My P2 README.md file")

	* Backward-engineer the lis4369_p2_requirements.txt file
	* Be sure to include at least two plots in your README.md file
	* Be sure to test your program using RStudio