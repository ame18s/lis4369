# LIS4369 Extensible Enterprise Solutions

## Alec Elsbernd

### Assignment 4 Requirements:

*Four Parts:*

1. Requirements:
    1. Code and run demo.py. (Note: *be sure* necessary packages are installed!)
    2. Then use it to backward-engineer the screenshots below it.
    3. When displaying the required graph (see code below), answer the following question: Why is the graph line split?
2. Be sure to test your program using both IDLE and Visual Studio Code
3. Questions
4. Bitbucket repo links:
    a. this assignment 

#### README.md file should include the following items:

* Assignment requirements, as per A1
* Screenshot as per examples below

#### Assignment Screenshots:

*Screenshot of a4_data_analysis_2 application running (Visual Studio Code):*

![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.1.PNG)Â ![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.2.PNG)
![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.3.PNG)Â ![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.4.PNG)
![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.5.PNG)Â ![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.6.PNG)
![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.7.PNGÂ ![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.8.PNG)
![AppÂ RunningÂ inÂ VisualÂ StudioÂ Screenshot](img/a4.9.PNG)Â 

*Screenshot of a4_data_analysis_2 application running (IDLE):*

![App Running in IDLE Screenshot|1148x1360,25%](img/a4.10.PNG) ![App Running in IDLE Screenshot|1148x1360,25%](img/a4.12.PNG)
![App Running in IDLE Screenshot|1148x1360,25%](img/a4.13.PNG) ![App Running in IDLE Screenshot|1148x1360,25%](img/a4.14.PNG)
![App Running in IDLE Screenshot|1148x1360,25%](img/a4.15.PNG) ![App Running in IDLE Screenshot|1148x1360,25%](img/a4.16.PNG)
![App Running in IDLE Screenshot|1148x1360,25%](img/a4.17.PNG) ![App Running in IDLE Screenshot|1148x1360,25%](img/a4.18.PNG)


*Screenshot of graph:*

![Graph after running App](img/graph.PNG)




