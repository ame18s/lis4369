# LIS4369

## Alec Elsbernd

### Project 1 Requirements:

 * Run demo.py.
 * If erros, more than likely missing installations.
 * Test Python Package Installer: pip freeze
 * Reserach how to do the following intsallations:
 	* pandas (only if missing) 
 	* pandas-datareader (only if missing)
 	* Matplotlib (only if missing)
 * Create at least three functions that are called by the program:
 	* main(): calls at least two other fuctions 
 	* get_requirments(): displays the program requirements.
 	* data_analysis_1(): displaysthe following data.

#### Project 1 Screenshots

![](img/main.PNG)


![](img/functions1.PNG)


![](img/functions2.PNG)


![](img/p1.1.PNG)


![](img/p1.2.PNG)


![](img/idle.PNG)

