# LIS4369

## Alec Elsbernd

### Assignment #3 Requirements:
* Calculate home interior paint cost (w/o primer).
* Must use float data type.
* Must use SQFT_PER_GALLON constant (350).
* Must use iteration structure (aka loop).
* Format, right-align numbers, and round to two decimal places.
* Create at least five functions that are called by the program:
	* main(): calls two other functions: get_requirments() and estimate_painting_cost().
	* get_requirments(): displays the program requirments.
	* estimate_painting_cost(): calculates interior home paintingm and calls print functions.
	* print_painting_estimate(): displays painting costs.
	* print_painting_percentage(): displays painting costs persentages.


#### Assignment Screenshots:


![a3 requirments](img/a3.3.PNG)

![vscode output](img/a3.1.png)

![a3 main function](img/a3.2.png)

![a3 code](img/a3.4.png)

![a3 idle output](img/a3.5.PNG)








