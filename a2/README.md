# LIS4369

## Alec Elsbernd

### Assignment #2 Requirements:

 - Must use float data type for user input.\n"
 - Overtime rate: 1.5 times hourly rate (hours over 40).\n"
 - Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
 - Must format currency with dollar sign, and round to two decimal places.\n"
 - Create at least three functions that are called by the program\n"
   	- main(): calls at least two other functions.\n"
	- get_requirements(): displays the program requirements.\n"
	- calculate_payroll(): calculates an individual one-week paycheck.\n")


#### Assignment Screenshots:



![customer sql statements](img/a2.1.png)



![company sql](img/a2.2.png)



![select statement](img/a2.3.png)



![select statement](img/a2.5.png)



![select statement](img/a2.4.png)


